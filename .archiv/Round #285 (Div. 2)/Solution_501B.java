import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Solution_501B {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(reader.readLine());

        Map<String, String> handles = new HashMap<String, String>();

        for (int i = 0; i < num; i++) {
            String strings[] = reader.readLine().split(" ");
            if(handles.containsKey(strings[0])) {
                handles.put(strings[1], handles.get(strings[0]));
                handles.remove(strings[0]);
            } else
                handles.put(strings[1], strings[0]);
        }

        System.out.println(handles.size());
        for(Map.Entry<String, String> handle : handles.entrySet())
            System.out.println(handle.getValue() + " " + handle.getKey());
    }
}
