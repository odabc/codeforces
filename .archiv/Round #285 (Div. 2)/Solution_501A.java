import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_501A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = reader.readLine().split(" ");
        int a = Integer.parseInt(strings[0]);
        int b = Integer.parseInt(strings[1]);
        int c = Integer.parseInt(strings[2]);
        int d = Integer.parseInt(strings[3]);

        int m1 = 3 * a / 10;
        int m2 = a - ((a / 250) * c);
        int m = m1 > m2 ? m1 : m2;

        int v1 = 3 * b / 10;
        int v2 = b - ((b / 250) * d);
        int v = v1 > v2 ? v1 : v2;

        if (m > v) System.out.println("Misha");
        else if (m < v) System.out.println("Vasya");
        else System.out.println("Tie");
    }
}
