import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_489A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int size = Integer.parseInt(reader.readLine());
        String[] strings = reader.readLine().split(" ");
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = Integer.parseInt(strings[i]);
        }
        int swaps = 0;
        strings = new String[size];

        for (int i = 0; i < size; i++) {
            int min = i;
            for (int j = i + 1; j < size; j++) {
                if (array[min] > array[j])
                    min = j;
            }
            if (i != min) {
                strings[swaps++] = (i + " " + min);
                array[i] = array[i] ^ array[min];
                array[min] = array[i] ^ array[min];
                array[i] = array[i] ^ array[min];
            }
        }
        System.out.println(swaps);
        for (int i = 0; i < swaps; i++) {
            System.out.println(strings[i]);
        }
    }
}
