import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution_489B {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int boys = Integer.parseInt(reader.readLine());
        String[] strings = reader.readLine().split(" ");
        int[] boySkill = new int[boys];
        for (int i = 0; i < boys; i++) {
            boySkill[i] = Integer.parseInt(strings[i]);
        }

        int girls = Integer.parseInt(reader.readLine());
        strings = reader.readLine().split(" ");
        int[] girlSkill = new int[girls];
        for (int i = 0; i < girls; i++) {
            girlSkill[i] = Integer.parseInt(strings[i]);
        }

        System.out.println(calc(boySkill, girlSkill, 1));
    }

    // вычисление количества пар совпадений значений с заданым отклонением, т. е. |a1 - a2| <= delta
    public static int calc(int[] a1, int[] a2, int delta) {
        Arrays.sort(a1);
        Arrays.sort(a2);

        int res = 0;
        for(int i = 0; i < a1.length; i++) {
            for(int j = 0; j < a2.length; j++) {
                if( Math.abs(a1[i] - a2[j]) <= delta ) {
                    res++;
                    // пара найдена -> исключаем ее из поиска
                    a1[i] = -5;
                    a2[j] = -10;
                }
            }
        }

        return res;
    }
}
