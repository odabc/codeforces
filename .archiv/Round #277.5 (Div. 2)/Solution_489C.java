import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_489C {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int len = Integer.parseInt(strings[0]);
        int sum = Integer.parseInt(strings[1]);

        // число возможно представить с данной разрядностью
        if ( (len == 0) || ((sum == 0) && (len > 1)) || (sum / len > 9) || ((sum / len == 9) && (sum % len > 0)) ) {
            System.out.println("-1 -1");
            System.exit(0);
        }

        int[] max = new int[len];
        int tmp = sum;
        for (int i = 0; i < len; i++) {
            if (tmp > 9) {
                max[i] = 9;
                tmp -= max[i];
            } else {
                max[i] = tmp;
                break;
            }
        }

        int[] min = new int[len];
        tmp = sum;
        for (int i = len - 1; i >= 0; i--) {
            if (tmp > 9) {
                min[i] = 9;
                tmp -= min[i];
            } else {
                min[0] = i > 0 ? 1 : tmp;
                min[i] += i > 0 ? tmp - 1 : 0;
                break;
            }
        }
        for(int i : min)
         System.out.print(i);

        System.out.print(" ");

        for(int i : max)
            System.out.print(i);
    }
}
