import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Solution_499B {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int lesionLength = Integer.parseInt(strings[0]);
        int dictLength = Integer.parseInt(strings[1]);

        Map<String, String> dict = new HashMap<String, String>();
        for (int i = 0; i < dictLength; i++) {
            strings= reader.readLine().split(" ");
            if(strings[0].length() <= strings[1].length()) {
                dict.put(strings[0], strings[0]);
                dict.put(strings[1], strings[0]);
            } else {
                dict.put(strings[0], strings[1]);
                dict.put(strings[1], strings[1]);
            }
        }

        strings = reader.readLine().split(" ");
        String outLecture = "";
        for (String word : strings) {
            outLecture += dict.get(word) + " ";
        }

        System.out.println(outLecture);
    }
}
