import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_499A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int moments = Integer.parseInt(strings[0]);
        int jump = Integer.parseInt(strings[1]);

        int duration = 0;
        int position = 1;

        for (int i = 0; i < moments; i++) {
            strings = reader.readLine().split(" ");
            int start = Integer.parseInt(strings[0]);
            int end = Integer.parseInt(strings[1]);

            duration += ( start - position ) % jump + end - start + 1;
            position = end + 1;
        }

        System.out.println(duration);
    }
}
