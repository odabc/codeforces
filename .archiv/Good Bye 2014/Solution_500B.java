import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_500B {
    static boolean[][] matrix;
    static int[] mutation;
    static boolean[] used;

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int size = Integer.parseInt(reader.readLine());

        String[] strings = reader.readLine().split(" ");
        mutation = new int[size];
        for (int i = 0; i < size; i++)
            mutation[i] = Integer.parseInt(strings[i]);

        matrix = new boolean[size][size];
        for (int i = 0; i < size; i++) {
            char[] arr = reader.readLine().toCharArray();
            for (int j = 0; j < size; j++)
                matrix[i][j] = arr[j] == '1';
        }

        for (int i = 0; i < size; i++) {
            used = new boolean[size];
            dfs(i);

            for (int j = i + 1; j < mutation.length; j++) {
                if(used[i] & used[j] & (mutation[i] > mutation[j]) ) {
                    mutation[i] = mutation[j] ^ mutation[i];
                    mutation[j] = mutation[j] ^ mutation[i];
                    mutation[i] = mutation[j] ^ mutation[i];
                }

            }
        }

        for(int mn : mutation)
            System.out.print(mn + " ");
        System.out.println();
    }

    static void dfs(int node) {
        used[node] = true;

        for (int i = 0; i < mutation.length; i++)
            if ( !used[i] && ( matrix[i][node] || matrix[node][i] ) )
                dfs(i);
    }

}
