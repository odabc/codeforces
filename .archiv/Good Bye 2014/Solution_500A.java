import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_500A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int size = Integer.parseInt(strings[0]);
        int end = Integer.parseInt(strings[1]);
        int start = 1;
        int pos = 1;

        strings = reader.readLine().split(" ");
        for (int i = 1; i <= size; i++) {
            int jump = Integer.parseInt(strings[i - 1]);
            if(pos == i)
                pos += jump;

            if(pos == end)  {
                System.out.println("YES");
                break;
            } else if (pos > end) {
                System.out.println("NO");
                break;
            }
        }
    }
}
