import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Solution_507A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int instr = Integer.parseInt(strings[0]);
        int teachDays = Integer.parseInt(strings[1]);

        strings = reader.readLine().split(" ");
        int[] instrTeach = new int[instr];
        for (int i = 0; i < instr; i++) {
            instrTeach[i] = Integer.parseInt(strings[i]);
        }

        int teach = 0;
        int days = 0;
        String out = "";

        for (int i = 0; i < instr; i++) {
            int min = instrTeach[0];
            int pos = 0;

            for (int j = 0; j < instr; j++) {
                if(min > instrTeach[j]) {
                    min = instrTeach[j];
                    pos = j;
                }
            }

            if(days + min <= teachDays) {
                days += min;
                teach++;
                out += (pos + 1) + " ";
                instrTeach[pos] = teachDays;
            } else
                break;
        }


        System.out.println(teach);
        System.out.println(out);
    }
}
