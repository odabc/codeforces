import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_507B {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int r = Integer.parseInt(strings[0]);
        int x1 = Integer.parseInt(strings[1]);
        int y1 = Integer.parseInt(strings[2]);
        int x2 = Integer.parseInt(strings[3]);
        int y2 = Integer.parseInt(strings[4]);

        double res = Math.sqrt( Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2) ) / (2 * r);

        System.out.println( (int) Math.ceil(res) );
    }
}
