import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_507C {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int h  = Integer.parseInt(strings[0]);      // высота двоичного дерева
        int fin  = Integer.parseInt(strings[1]);    // номер выхода

        long left = 1;                       // первый выход
        long right = (long) Math.pow(2, h); // последний выход


        Boolean onLeft = false;                  // переключатель право-лево

        for (int i = 0; i < h; i++)
        {
            long mid = (left + right) / 2;  // пройдена половина выходов -> разделение дерева пополам
            if (fin <= mid && !onLeft)
            {
                right = mid;
                onLeft = true;
                continue;
            }

            if (fin <= mid && onLeft)
            {
                fin += right - mid;
                left = mid + 1;
                onLeft = true;
                continue;
            }

            if (fin > mid && !onLeft)
            {
                left = mid + 1;
                onLeft = false;
                continue;
            }

            if (fin > mid && onLeft)
            {
                fin -= right - mid;
                right = mid;
                onLeft = false;
            }
        }

        long ans = 0;
        for (int i = 0; i < h; i++)
        {
            ans += fin;
            fin = (fin + 1) / 2;
        }

        System.out.println(ans);
    }
}
