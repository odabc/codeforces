import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_58A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String st = reader.readLine();
        char[] chars = {'h', 'e', 'l', 'l', 'o'};
        int pnt = 0;

        for (int i = 0; ( i < st.length() ) && ( pnt < 5 ); i++) {
            if(st.charAt(i) == chars[pnt])
                pnt++;
        }

        System.out.println((pnt == 5) ? "YES" : "NO");
    }
}
