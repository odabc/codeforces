import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_282A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int operations = Integer.parseInt(reader.readLine());
        int X = 0;

        for(int i=0; i < operations; i++) {
            char chars[] = reader.readLine().toCharArray();
            if((chars[0] == '+') || (chars[1] == '+') || (chars[2] == '+'))
                X++;
            else X--;
        }

        System.out.println(X);
    }
}
