import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_112A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] chars1 = reader.readLine().toCharArray();
        char[] chars2 = reader.readLine().toCharArray();

        for(int i=0; i < chars1.length; i++) {
            char ch1 = Character.toUpperCase(chars1[i]);
            char ch2 = Character.toUpperCase(chars2[i]);
            if(ch1 == ch2)
                continue;
            else if(ch1 > ch2) {
                System.out.println(1);
                System.exit(0);
            } else {
                System.out.println(-1);
                System.exit(0);
            }
        }

        System.out.println(0);
    }
}
