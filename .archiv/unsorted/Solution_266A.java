import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_266A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(reader.readLine());
        char[] stones = reader.readLine().toCharArray();
        int remove = 0;

        for(int i=0; i < number - 1; i++)
            if (stones[i] == stones[i + 1]) remove++;

        System.out.println(remove);
    }
}
