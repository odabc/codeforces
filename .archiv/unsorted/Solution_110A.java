import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_110A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] chars = reader.readLine().toCharArray();
        int count = 0;

        for(char ch : chars)
            if((ch == '7') || (ch == '4'))
                count++;

        System.out.println(((count == 7) || (count == 4)) ? "YES" : "NO");
    }
}
