import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Solution_236A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] name = reader.readLine().toCharArray();
        String good = "CHAT WITH HER!";
        String bad = "IGNORE HIM!";
        Set<Character> chSet = new HashSet<Character>();

        for(char ch : name)
            chSet.add(ch);

        System.out.println((chSet.size() % 2 == 0) ? good : bad);
    }
}
