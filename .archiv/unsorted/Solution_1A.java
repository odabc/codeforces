import java.io.BufferedReader;
import java.io.InputStreamReader;
public class Solution_1A {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader (new InputStreamReader(System.in));
        String strings[] = input.readLine().split(" ");

        int n = Integer.parseInt(strings[0]);
        int m = Integer.parseInt(strings[1]);
        int a = Integer.parseInt(strings[2]);

        long x = n/a;
        long y = m/a;

        if (n%a != 0) x++;
        if (m%a != 0) y++;

        System.out.println(x * y);
    }
}
