import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Solution_228A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = reader.readLine().split(" ");

        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < 4 /*strings.length*/; i++)
            set.add(Integer.parseInt(strings[i]));

//        System.out.println((4 - set.size()) >=0 ? 4 - set.size() : 0);
        System.out.println(4 - set.size());
    }
}
