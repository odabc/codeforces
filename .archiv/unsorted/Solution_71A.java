import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_71A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.parseInt(reader.readLine());

        String[] strings = new String[count];
        for(int i = 0; i < count; i++) strings[i] = reader.readLine();

        for(int i = 0; i < count; i++)
            if (strings[i].length() > 10) {
                String st = strings[i];
                strings[i] = "" + st.charAt(0) + (st.length() - 2) + st.charAt(st.length() - 1);
            }

        for(String st : strings)
            System.out.println(st);
    }
}
