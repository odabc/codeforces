import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_136A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int numb = Integer.parseInt(reader.readLine());
        String[] strings = reader.readLine().split(" ");
        int[] to = new int[numb];
        int[] from = new int[numb];

        for (int i = 0; i < numb; i++)
            to[i] = Integer.parseInt(strings[i]);

        for (int i = 0; i < numb; i++)
            from[to[i] - 1] = i + 1;

        for(int i : from)
            System.out.printf("%d ", i);
    }
}
