import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_82A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        String[] startTurn = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};

        System.out.println(startTurn[search(n) - 1]);
    }

    public static int search(int n) {
        if (n <= 5)
            return n;
        else
            return search((n - 5) / 2 + (n - 5) % 2);
    }
}
