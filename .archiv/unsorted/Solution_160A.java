import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution_160A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int amount = Integer.parseInt(reader.readLine());
        String strings[] = reader.readLine().split(" ");
        int[] nominals = new int[amount];
        int maxMoney = 0;

        for (int i = 0; i < amount; i++) {
            nominals[i] = Integer.parseInt(strings[i]);
            maxMoney += nominals[i];
        }

        maxMoney = (maxMoney / 2) + 1;
        Arrays.sort(nominals);

        int grabMoney = 0;
        int grab = 0;

        while (grabMoney < maxMoney) {
            grab++;
            grabMoney += nominals[amount - grab];
        }

        System.out.println(grab);
    }
}
