import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_460A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = reader.readLine().split(" ");
        int n = Integer.parseInt(strings[0]);
        int m = Integer.parseInt(strings[1]);
        int days = 0;
        int socks = n;

        days = calc(socks, m);
/*
        while(socks >= m) {
            days += (socks / m) * m;
            socks = socks / m + socks % m;
        }
        days += socks;
*/

        System.out.println(days);
    }

    public static int calc(int socks, int m) {
        return calc(0, socks, m);
    }

    public static int calc(int days, int socks, int m) {
        return (socks >= m) ? calc(days + (socks / m) * m, socks / m + socks % m, m) : days + socks;
    }
}
