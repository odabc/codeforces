import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_379A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = reader.readLine().split(" ");
        int a = Integer.parseInt(strings[0]);
        int b = Integer.parseInt(strings[1]);
        int hours = 0;
        int last = a;

        while(last >= b) {
            hours += (last / b) * b;
            last = last / b + last % b;
        }
        hours += last;

        System.out.println(hours);
    }
}
