import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_131A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] string = reader.readLine().toCharArray();
        Boolean toChange = true;

        for(int i=1; i < string.length; i++)
            if (string[i] == Character.toUpperCase(string[i]))
                toChange &= true;
            else toChange &= false;

        if(toChange) {
            if (string[0] == Character.toUpperCase(string[0]))
                string[0] = Character.toLowerCase(string[0]);
            else
                string[0] = Character.toUpperCase(string[0]);

            for (int i = 1; i < string.length; i++)
                string[i] = Character.toLowerCase(string[i]);
        }

        for(char ch : string)
            System.out.print(ch);
    }
}
