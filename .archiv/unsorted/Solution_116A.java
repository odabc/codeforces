import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_116A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int stops = Integer.parseInt(reader.readLine());
        int maxInBusPeople = 0;
        int inBusPeople = 0;

        for(int i=0; i < stops; i++) {
            String strings[] = reader.readLine().split(" ");
            int out = Integer.parseInt(strings[0]);
            int in = Integer.parseInt(strings[1]);
            inBusPeople -= out;
            inBusPeople += in;
            maxInBusPeople = (maxInBusPeople > inBusPeople) ? maxInBusPeople : inBusPeople;
        }

        System.out.println(maxInBusPeople);
    }
}
