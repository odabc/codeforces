import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_148A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int k = Integer.parseInt(reader.readLine());
        int l = Integer.parseInt(reader.readLine());
        int m = Integer.parseInt(reader.readLine());
        int n = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());

        int hit = 0;

        for (int i=1; i <= d; i++) {
            if((i % k) == 0)
                hit++;
            else if((i % l) == 0)
                hit++;
            else if((i % m) == 0)
                hit++;
            else if((i % n) == 0)
                hit++;
        }

        System.out.println(hit);
    }
}
