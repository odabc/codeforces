import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_271A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int startYear = Integer.parseInt(reader.readLine());
        int needYear = startYear + 1;
        int[] digits = toDigit(needYear);

     //   while(!diff(toDigit(needYear)))
        while(!diffSt(Integer.toString(needYear)))
            needYear++;

        System.out.println(needYear);
    }

    public static Boolean diffSt(String st) {
        for (int i = 0; i < st.length() ; i++)
            if(st.substring(i + 1).contains(st.charAt(i) + ""))
                return false;
        return true;
    }

    public static Boolean diff(int[] digits) {
        if((digits[0] == digits[1]) || (digits[0] == digits[2]) || (digits[0] == digits[3]))
            return false;
        else if ((digits[1] == digits[2]) || (digits[1] == digits[3]))
            return false;
        else if (digits[2] == digits[3])
            return false;

        return true;
    }

    public static int[] toDigit(int i) {
        int[] digits = new int[4];

        digits[0] = i / 1000;
        digits[1] = (i % 1000) / 100;
        digits[2] = ((i % 1000) % 100) / 10;
        digits[3] = ((i % 1000) % 100) % 10;

        return digits;
    }
}
