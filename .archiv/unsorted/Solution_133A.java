import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_133A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char string[] = reader.readLine().toCharArray();
        char[] printCommands = {'H', 'Q', '9'};

        for(char ch : string)
            for(char command : printCommands)
                if (ch == command) {
                    System.out.println("YES");
                    System.exit(0);
                }

        System.out.println("NO");
    }
}
