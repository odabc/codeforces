import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_50A {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = input.readLine().split(" ");
        int m = Integer.parseInt(strings[0]);
        int n = Integer.parseInt(strings[1]);
        int res = 0;

        if(m < 2 && n < 2) {
            System.out.println(0);
            System.exit(0);
        }

        if(m % 2 == 1) {
            res = ((n / 2) * m);
            res += (n % 2 == 1) ? m / 2 : 0;
        } else res = (m / 2) * n;

        System.out.println(res);
    }
}
