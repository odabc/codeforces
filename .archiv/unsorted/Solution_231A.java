import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_231A {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        int taskNumber = Integer.parseInt(input.readLine());
        int taskSolved = 0;

        for(int i = 0; i < taskNumber; i++) {
            String strings[] = input.readLine().split(" ");
            int solved = 0;
            for(int j = 0; j < 3; j++)
                solved += Integer.parseInt(strings[j]);
            if(solved >= 2) taskSolved++;
        }

        System.out.println(taskSolved);
    }
}
