import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_467A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int rooms = Integer.parseInt(reader.readLine());
        int free = 0;

        for (int i = 0; i < rooms; i++) {
            String pair[] = reader.readLine().split(" ");
            int ppl = Integer.parseInt(pair[0]);
            int max = Integer.parseInt(pair[1]);

            if (max - ppl >= 2) free++;
        }

        System.out.println(free);
    }
}
