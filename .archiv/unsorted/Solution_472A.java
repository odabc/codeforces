import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_472A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = a / 2;
        int c = a - b;

/*
        if( (a % 2) == 0 )
            System.out.println((a - 6) + " " + 6);
        else
            System.out.println((a - 9) + " " + 9);
*/
        while( !( ( (b % 2) == 0 || (b % 3) == 0 ) && ( (c % 2) == 0 || (c % 3) == 0 ) ) ) {
            b++;
            c--;
        }
        System.out.println(b + " " + c);

    }
}
