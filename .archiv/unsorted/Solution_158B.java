import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_158B {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int groups = Integer.parseInt(reader.readLine());
        String[] groupValue = reader.readLine().split(" ");

        int[] groupCount = {0, 0, 0, 0};
        for(String st : groupValue)
            switch (Integer.parseInt(st)) {
            case 1:
                groupCount[0]++;
                break;
            case 2:
                groupCount[1]++;
                break;
            case 3:
                groupCount[2]++;
                break;
            case 4:
                groupCount[3]++;
                break;
            }
        //combine 3 & 1
        if((groupCount[0] > 0) && (groupCount[2] > 0))
            if(groupCount[0] > groupCount[2])
                groupCount[0] -= groupCount[2];
            else
                groupCount[0] = 0;

        //combine 2 & 2, 2 & 1
        if(groupCount[1] % 2 == 1) {
            groupCount[1] = groupCount[1] / 2 + 1;
            if(groupCount[0] > 0) groupCount[0]--;
            if(groupCount[0] > 0) groupCount[0]--;
        } else
            groupCount[1] = groupCount[1] / 2;

        //combine 1 & 1
        if(groupCount[0] % 4 > 0)
            groupCount[0] = groupCount[0] / 4 + 1;
        else
            groupCount[0] = groupCount[0] / 4;

        System.out.print(groupCount[3] + groupCount[2] + groupCount[1] + groupCount[0]);
    }
}
