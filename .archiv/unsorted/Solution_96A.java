import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_96A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char plList[] = reader.readLine().toCharArray();
        int seq = 1;

        for(int i=0; i < plList.length - 1; i++) {
            if(plList[i] == plList[i + 1])
                seq++;
            else
                seq = 1;
            if(seq >= 7) {
                System.out.println("YES");
                System.exit(0);
            }
        }

        System.out.println("NO");
    }
}
