import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_158A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] param = reader.readLine().split(" ");
        int n = Integer.parseInt(param[0]);
        int k = Integer.parseInt(param[1]) - 1;

        int[] array = new int[n];
        param = reader.readLine().split(" ");
        for(int i = 0; i < n; i++) array[i] = Integer.parseInt(param[i]);

        int nextRound = 0;
        for(int i : array) {
            if ((i > 0) & (i >= array[k])) nextRound++;
        }

        System.out.println(nextRound);
    }
}
