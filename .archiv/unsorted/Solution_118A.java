import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_118A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] input = reader.readLine().toLowerCase().toCharArray();
        char[] output = new char[input.length * 2];

        int pos = 0;
        for(int i=0; i < input.length; i++) {
            if(!vowel(input[i])) {
                output[pos++] = '.';
                output[pos++] = input[i];
            }
        }

        for(char ch : output)
            if (ch != '\u0000')
                System.out.print(ch);
    }

    public static Boolean vowel(char tst) {
        char[] vowels = {'a', 'o', 'y', 'e', 'u', 'i'};
        for(char ch : vowels)
            if (ch == tst) return true;
        return false;
    }
}

