import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_119A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = reader.readLine().split(" ");
        int a = Integer.parseInt(strings[0]);
        int b = Integer.parseInt(strings[1]);
        int n = Integer.parseInt(strings[2]);

        while(true) {
            try {
                n = play(a, n, 1);
                n = play(b, n, 0);
            }
            catch (IllegalStateException e)  {
                System.out.println(e.getLocalizedMessage());
                System.exit(0);
            }
        }
    }

    public static int play(int a, int n, int code) {
        int gcd = gcd(a, n);
        if (n >= gcd)
            return n - gcd;
        else
            throw new IllegalStateException("" + code);
    }

    public static int gcd(int a, int b) {
        int min = a <= b ? a : b;

        for (int i = min; i >= 1; i--)
            if (((a % i) == 0) && ((b % i) == 0))
                return i;
        return 1;
    }
}
