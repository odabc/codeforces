import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_281A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] string = reader.readLine().toCharArray();

        string[0] = Character.toUpperCase(string[0]);
        for(char ch :string)
            System.out.print(ch);
    }
}
