import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_339A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] input = reader.readLine().toCharArray();
        int[] n = new int[4];

        for(char ch : input)
            switch (ch) {
                case '+':
                    n[0]++;
                    break;
                case '1':
                    n[1]++;
                    break;
                case '2':
                    n[2]++;
                    break;
                case '3':
                    n[3]++;
                    break;

            }

        for (int i = 1; i <= 3; i++)
            for (int j = 0; j < n[i]; j++)
                System.out.print(i + (n[0]-- > 0 ? "+" : ""));

        System.out.println();
    }
}
