import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_4A {
    public static void main(String[] args) throws Exception{
        BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
        int weight = Integer.parseInt(reader.readLine());
        if (weight <= 2)
                System.out.println("NO");
        else
            System.out.println((weight %2 == 0) ? "YES" : "NO");
    }
}
