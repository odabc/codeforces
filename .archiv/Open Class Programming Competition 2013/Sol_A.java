/**
 * Created by asus on 21.05.15.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Sol_A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        long R = Long.parseLong(strings[0]);
        long D = Long.parseLong(strings[1]);
        long p = Long.parseLong(strings[2]);
        long n = Long.parseLong(strings[3]);

        long res = (R - D * n) % p;
        if (res < 0)
            res += p;

        System.out.println(res);
    }
}
