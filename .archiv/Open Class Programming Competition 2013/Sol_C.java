/**
 * Created by asus on 21.05.15.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Sol_C {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int len = Integer.parseInt(reader.readLine());
        String[] strings = reader.readLine().split(" ");
        int[] seq = new int[len];

        for (int i = 0; i < len; i++) {
            seq[i] = Integer.parseInt(strings[i]);
        }

        int res = 0;
        for (int i = 0; i < len-2; i++) {
            for (int j = i+2; j < len; j++) {
                if (seq[j] > seq[i])
                    for (int k = i+1; k < j; k++)
                        if ((seq[i] > seq[k]) && (seq[i] + seq[j] + seq[k]) % 3 == 0) {
                                res++;
                                break;
                        }
            }
        }

        System.out.println(res);
    }
}
