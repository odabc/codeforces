import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution_505A {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] chars = reader.readLine().toCharArray();

        ArrayList<char[]> varChars = new ArrayList<char[]>();
        for (int i = 0; i <= chars.length; i++) {
            char[] var = new char[chars.length + 1];
            var[chars.length - i] = (i <= chars.length / 2) ? chars[i] : chars[i - 1];

            System.arraycopy(chars, 0, var, 0, chars.length - i);
            System.arraycopy(chars, chars.length - i, var, chars.length - i + 1, i);
            varChars.add(var);
        }

        for(char[] test : varChars) {
            if(palindrome(test)) {
                for(char ch : test)
                    System.out.print(ch);
                System.out.println();
                System.exit(0);
            }
        }

        System.out.println("NA");
    }

    public static Boolean palindrome(char[] test) {
        for (int i = 0; i < test.length / 2; i++) {
            if(test[i] != test[test.length - 1 - i])
                return false;
        }
        return true;
    }


//        int sz = chars.length;
//        char[] out = new char[sz + 1];
//        int ins = 0;
//
//        out[sz / 2] = chars[sz / 2]; // set middle character
//
//        for (int i = 0; i < out.length / 2; i++) {
//            out[i] = chars[i];
//            out[sz - i] = chars[i];
//            if(chars[i] != chars[sz - i - 1 + ins])
//                ins++;
//
//            // ignore previous and start new cycles
//            if (ins > 1) {
//                ins = 0;
//                for (i = out.length - 1; i >= out.length / 2; i--) {
//                    out[i] = chars[i - 1];
//                    out[sz - i] = chars[i - 1];
//                    if(chars[i - 1] != chars[sz - i - ins])
//                        ins++;
//                    if (ins > 1)
//                        break; // not found
//                }
//                break; // not found
//            }
//        }
//
//        if(ins <= 1) { // check if found
//            for (char ch : out)
//                System.out.print("" + ch);
//            System.out.println();
//
//        } else
//            System.out.println("NA");
}

