import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution_505B {
    static boolean[] used;
    static ArrayList<Node>[] graph;

    static class Node {
        int id;
        int color;

        public Node(int id, int color) {
            this.id = id;
            this.color = color;
        }
    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = reader.readLine().split(" ");
        int nodes = Integer.parseInt(strings[0]);   //количество вершин графа
        int edges = Integer.parseInt(strings[1]);   // количество ребер графа

        graph = new ArrayList[nodes];
        for(int i = 0; i < nodes; i++)
            graph[i] = new ArrayList<Node>();

        // построение матрицы смежности
        for (int i = 1; i <= edges; i++) {
            String edge[] = reader.readLine().split(" ");
            int node1 = Integer.parseInt(edge[0]) - 1;
            int node2 = Integer.parseInt(edge[1]) - 1;
            int color = Integer.parseInt(edge[2]);

            graph[node1].add(new Node(node2, color));
            graph[node2].add(new Node(node1, color));
        }

        // обработка запросов по нахождению количества путей в графе между вершинами
        int requests = Integer.parseInt(reader.readLine());
        for (int i = 0; i < requests; i++) {
            String edge[] = reader.readLine().split(" ");
            int node1 = Integer.parseInt(edge[0]) - 1;
            int node2 = Integer.parseInt(edge[1]) - 1;
            int num = 0;

            // вызов рекурсивного метода поиска путей - поиск в глубину по матрице смежности
            for (int j = 1; j <= edges; j++) {
                used = new boolean[nodes];  // обнуление меток обработаных вершин
                if ( dfs(j, node1, node2) )
                    num++;
            }

            System.out.println(num);
        }
    }

    // Поиск в глубину (Depth-first search, DFS) заключается в следующем:
    // фиксируем текущую вершину, помечаем ее как посещенную,
    // и пока есть смежные с ней не посещенные вершины, рекурсивно их обходим.
    // Для определения того, что вершина посещена, обычно используется массив флагов.
    public static boolean dfs(int color, int node1, int node2) { // дополнительный параметр - цвет ребра

        if(node1 == node2) return true;
        used[node1] = true;
        ArrayList<Node> edges = graph[node1];
        for(Node edge : edges){
            // проверка на цвет && метку обработаной верщины
            // если не обработана - рекурсивный вызов метода обработки
            // в качестве базовой - связаная не обойденная вершина
            if(edge.color == color && !used[edge.id]){
                if(dfs(color, edge.id, node2)) return true;
            }
        }
        return false;
    }

}

