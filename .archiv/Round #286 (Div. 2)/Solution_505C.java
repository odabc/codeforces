import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
//import java.util.Scanner;

public class Solution_505C {
//    static int[] data;      // местоположение и количество кладов
//    static int[][] memo;    // матрица траекторий
//    static int d;           // величина прыжка
//
//    public static void main(String[] args)
//    {
//        Scanner input = new Scanner(System.in);
//        int n = input.nextInt();    // кол-во кладов
//        d = input.nextInt();        // длина стартового прыжка
//
//        data = new int[30001];      // острова с кладами
//        for(int i = 0; i<n; i++) data[input.nextInt()]++;
//
//        memo = new int[30001][500]; // матрица траекторий
//        for(int[] A : memo) Arrays.fill(A, -1);
//
//        System.out.println(go(d, d));
//    }
//
//    static int go(int at, int last) // at - текущая позиция, last - величина перемещения
//    {
//        if(at > 30000) return 0;    // текущая позиция больше чем кол-во островов
//        //System.out.println(at+" "+last);
//
//        if(memo[at][last - d + 251] != -1)  //
//            return memo[at][last - d + 251];
//
//        int res = go(at+last, last); // длина прыжка не меняется
//
//        if(last > 1)
//            res = Math.max(res, go(at+last-1, last-1)); // длина прыжка - 1
//
//        res = Math.max(res, go(at+last+1, last+1)); // длина прыжка + 1
//
//        res += data[at];                            // проверка наличия клада в текущей позиции
//        memo[at][last - d + 251] = res;
//
//        return res;
//    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] strings = reader.readLine().split(" ");
        int stones = Integer.parseInt(strings[0]);
        int jump = Integer.parseInt(strings[1]);

        int endIsland = 0;
        int[] islandsStones = new int[30001];
        for (int i = 0; i < stones; i++) {
            int island = Integer.parseInt(reader.readLine());
            endIsland = endIsland > island ? endIsland : island;

            islandsStones[island]++;
        }

        int takeStones = islandsStones[jump];
        MyArray nodes = new MyArray();
        nodes.add(jump, jump, islandsStones[jump]);

        while (nodes.size() > 0) {
            MyArray newNodes = new MyArray();

            for (int i = 0; i < nodes.size(); i++) {
                int[] curNode = nodes.getInt(i);
                if (curNode[1] <= 2) {
                    stones = curNode[2];
                    for (int j = curNode[0]; j <= endIsland; j++)
                        stones += islandsStones[j];
                    takeStones = (takeStones > stones) ? takeStones : stones;
                    continue;
                }

                for (int j = -1; j <= 1; j++) {
                    int id = curNode[0] + curNode[1] + j;
                    jump = curNode[1] + j;
                    stones = curNode[2] + islandsStones[id];

                    if ((id > curNode[0]) && (id <= endIsland)) {
                        newNodes.add(id, jump, stones);
                        takeStones = (takeStones > stones) ? takeStones : stones;
                    }
                }
            }
            nodes = newNodes;
        }

        System.out.println(takeStones);
    }

    public static class MyArray {
        private int[] array = new int[12];
        private int size = 0;

        public int size() {
            return size;
        }

        public void add(int a, int b, int c) {
            int pos = size * 3;
            if ( pos  >= array.length) {
                array = Arrays.copyOf(array, array.length * 2);
            }
            size++;
            array[pos++] = a;
            array[pos++] = b;
            array[pos] = c;
        }

        public int[] getInt(int index) {
            if (index < 0 || index >= size)
                throw new IndexOutOfBoundsException();
            int pos = 3 * index;
            return new int[] { array[pos++], array[pos++], array[pos] };
        }
    }

}
