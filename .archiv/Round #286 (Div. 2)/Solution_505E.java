import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution_505E {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String strings[] = reader.readLine().split(" ");
        int all = Integer.parseInt(strings[0]);                 // общее количество бамбуков
        int days = Integer.parseInt(strings[1]);                // общее количество дней
        int kicks = Integer.parseInt(strings[2]);               // количество ударов в день
        long damage = Long.parseLong(strings[3]);               // сила удара

        long[][] bamboo = new long[all][2];                     // массив пар рост, скорость роста: инициализация
        for (int i = 0; i < all; i++) {
            String param[] = reader.readLine().split(" ");
            bamboo[i][0] = Long.parseLong(param[0]);
            bamboo[i][1] = Long.parseLong(param[1]);
        }

        int[] kicksInDay = new int[days];                       // массив использования ударов по дням
        long loHeight = 0, resultHeight = Long.MAX_VALUE, midHeight = resultHeight / 2;

        while (resultHeight - loHeight > 1) {
            int nowKicks = 0, nowDay = 0;
            Boolean fault = false;

            for (int i = 0; i < days; i++) kicksInDay[i] = 0;   // обнуляем массив ударов

            for (int i = 0; i < all && !fault; i++) {           // проход по всем бамбукам, если выполнены все условия
                long maxHeight = bamboo[i][0] + bamboo[i][1] * days;    // максимальная высота бамбука
                long nowHeight = bamboo[i][0];                          // текушая высота бамбука
                long needKicks, lastHeight, d = 0;

                if (maxHeight <= midHeight)               // продолжаем если высота меньше среднего значения высоты
                    continue;

                needKicks = (maxHeight - midHeight) / damage;     // количество ударов для устранения отклонения от среднего
                lastHeight = (maxHeight - midHeight) % damage;     // остаток после нанесения ударов

                nowKicks += (int) needKicks;                   // + количество нанесенных ударов
                if (lastHeight > 0) nowKicks++;           // если остаток > 0 + 1 удар

                if (nowKicks > days * kicks) {   // ударов не хватило до достижения среднего показателя
                    fault = true;
                    break;
                }

                if (lastHeight > 0) {                // обработка остатка от бамбука после нанесения ударов
                    if (nowHeight >= lastHeight) {   // начальная высота >= остатку после ударов до доведения до среднего уровня
                        kicksInDay[0]++;             // +1 удар в 1-й день
                        nowHeight = 0 > (nowHeight - damage) ? 0 : nowHeight - damage;    // либо 0, либо остаток после удара
                    } else {
                        long needDays = (lastHeight - nowHeight + bamboo[i][1] - 1) / bamboo[i][1];

                        if (needDays >= days) {    // превышено необходимое количество дней
                            fault = true;
                            break;
                        } else {
                            kicksInDay[nowDay]++;
                            d = needDays;
                            nowHeight += needDays * bamboo[i][1];
                            nowHeight =  ( 0 >= (nowHeight - damage) ) ? 0 : nowHeight - damage;
                        }
                    }
                }

                while (needKicks != 0) {
                    long needDays;

                    if (d >= days) {            // превышено необходимое количество дней
                        fault = true;
                        break;
                    }

                    if (nowHeight >= damage) {
                        long t = ( nowHeight / damage < needKicks ) ? nowHeight / damage : needKicks;

                        kicksInDay[(int) d] += t;
                        nowHeight -= damage * t;
                        needKicks -= t;
                    }

                    needDays = (damage - nowHeight + bamboo[i][1] - 1) / bamboo[i][1];
                    d += needDays;
                    nowHeight += needDays * bamboo[i][1];
                }
            }

            for (int i = days - 1; i >= 0; i--) {
                nowDay += kicksInDay[i];

                if (nowDay > (days - i) * kicks) {
                    fault = true;
                    break;
                }
            }

            if (!fault) {
                resultHeight = midHeight;
                midHeight = (loHeight + resultHeight) / 2;
            } else {
                loHeight = midHeight;
                midHeight = (loHeight + resultHeight) / 2;
            }
        }

        System.out.println(resultHeight);
    }
}
